function mostrar() {
    //Aqui se obtiene el objeto del select
    var select = document.getElementById("numT");
    //Aqui se obtiene el elemento seleccionado dentro del select
    var numMult = select.options[select.selectedIndex].value;
    //Variable donde se almacena el resultado de la multiplicación
    var result = 0;

    //Se borra todo detro del imgContainer
    while (document.getElementById('imgContainer').firstChild) {
        document.getElementById('imgContainer').removeChild(document.getElementById('imgContainer').firstChild);
    }

    //Ciclo para multiplicar
    for (let i = 0; i <= 10; i++) {
        console.log(numMult);
        result = i * numMult;
        //Se crea una etiqueta img
        var img = document.createElement('img');
        var imgX = document.createElement('img');
        var imgN2 = document.createElement('img');
        var imgIg = document.createElement('img');
        var imgR1 = document.createElement('img');
        var imgR2 = document.createElement('img');
        var imgR3 = document.createElement('img');



        //Se le asigna la ruta a la etiqueta creada
        img.src = "../img/" + numMult + ".png";
        imgX.src = "../img/x.png";
        imgN2.src = "../img/" + i + ".png";
        imgIg.src = "../img/=.png"
        if (result > 10 && result < 99) {
            // Crear un array para almacenar las posiciones
            let posiciones = [];
            let str = result.toString();

            // Recorrer el string
            for (let i = 0; i < str.length; i++) {
                // Añadir el valor de la posición al array
                posiciones[i] = str[i];
            }
            //Asignar la ruta dependiendo del valor del arreglo

            imgR1.src = "../img/" + posiciones[0] + ".png"
            
            imgR2.src = "../img/" + posiciones[1] + ".png"

        }
        else if(result > 99){
            // Crear un array para almacenar las posiciones
            let posiciones = [];
            let str = result.toString();

            // Recorrer el string
            for (let i = 0; i < str.length; i++) {
                // Añadir el valor de la posición al array
                posiciones[i] = str[i];
            }
            //Asignar la ruta dependiendo del valor del arreglo

            imgR1.src = "../img/" + posiciones[0] + ".png"
            imgR2.src = "../img/" + posiciones[1] + ".png"
            imgR3.src = "../img/" + posiciones[2] + ".png"
        }

        //Se inserta la etiqueta al documento HTML
        document.getElementById('imgContainer').appendChild(img);
        document.getElementById('imgContainer').appendChild(imgX);
        document.getElementById('imgContainer').appendChild(imgN2);
        document.getElementById('imgContainer').appendChild(imgIg);
        if(result <= 10){
            imgR1.src = "../img/" + result + ".png";
            document.getElementById('imgContainer').appendChild(imgR1);
        }
        else if (result > 10 && result < 100) {
            document.getElementById('imgContainer').appendChild(imgR1);
            document.getElementById('imgContainer').appendChild(imgR2);
        }
        else if(result > 99){
            document.getElementById('imgContainer').appendChild(imgR1);
            document.getElementById('imgContainer').appendChild(imgR2);
            document.getElementById('imgContainer').appendChild(imgR3);
        }


        //Se inserta un br para la separacion entre operación
        var br = document.createElement('br');
        document.getElementById('imgContainer').appendChild(br);

    }
}