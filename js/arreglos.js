// Declaración de arreglo
var arreglo = [19, 20, 3, 2, 6, 3, 3, 9, 50];

// Mostrar los elementos del arreglo
function mostrarArray() {
    for (let con = 0; con < arreglo.length; con++) {
        console.log(con + ":" + arreglo[con]);
    }
}

// Función que retorna el promedio de los elementos del arreglo
function promedio() {
    let prom = 0;
    for (let con = 0; con < arreglo.length; con++) {
        prom += arreglo[con];
    }
    return prom / arreglo.length;
}

// Realizar una función que regrese un valor entero que represente la cantidad de números pares en el arreglo
function pares() {
    let par = 0;
    for (let con = 0; con < arreglo.length; con++) {
        if (arreglo[con] % 2 === 0) {
            par += 1;
        }
    }
    return par;
}

// Función que regresa el número mayor contenido en el array
function mayor(array) {
    let may = 0;
    for (let con = 0; con < array.length; con++) {
        if (array[con] > may) {
            may = array[con];
        }
    }
    return may;
}

// Función para encontrar el número menor
function menor(array) {
    let men = array[0];
    for (let con = 0; con < array.length; con++) {
        if (array[con] < men) {
            men = array[con];
        }
    }
    return men;
}

// Función que muestra si el porcentaje de los elementos del arreglo es simétrico o no
function porcentajeSimetria(array) {
    // Contar el número de elementos pares e impares en el arreglo
    let pares = 0;
    let impares = 0;
    for (let i = 0; i < array.length; i++) {
        if (array[i] % 2 === 0) {
            pares++;
        } else {
            impares++;
        }
    }
    // Calcular el porcentaje de simetría
    let porcentajeSimetria = (pares / array.length) * 100;
    // Devolver el porcentaje de simetría
    return porcentajeSimetria;
}

// Función para sacar el porcentaje de números pares del arreglo
function porcentajePares(array) {
    let contador = 0;
    for (let i = 0; i < array.length; i++) {
        if (array[i] % 2 === 0) {
            contador++;
        }
    }
    let porcentajePare = ((contador / array.length) * 100).toFixed(2);
    return porcentajePare;
}

// Función para sacar el porcentaje de números impares del arreglo
function porcentajeImpares(array) {
    let contador = 0;
    for (let i = 0; i < array.length; i++) {
        if (array[i] % 2 !== 0) {
            contador++;
        }
    }
    let porcentajeImpare = ((contador / array.length) * 100).toFixed(2);
    return porcentajeImpare;
}

/* Diseñe una función que recibe un valor numérico que indica la cantidad de elementos del arreglo
   El arreglo deberá llenarse con valores aleatorios en el rango del 0 al 1000 posteriormente
   mostrará el arreglo */
function nums(rango) {
    const cmbAleatorio = document.getElementById('cmbNumeros');
    let arr = [];
    for (let cont = 0; cont < rango; cont++) {
        arr[cont] = Math.floor(Math.random() * 1000);

        let option = document.createElement('option');
        option.value = arr[cont];
        option.innerHTML = arr[cont];
        cmbAleatorio.appendChild(option);
    }
    return arr;
}

// Función para sacar el promedio de los números del arreglo
function promNumero(tNums) {
    let suma = 0;
    for (let i = 0; i < tNums.length; i++) {
        suma += parseInt(tNums[i]);
    }
    let prom = suma / tNums.length;
    return prom;
}

// Función para encontrar el número mayor del arreglo y mostrar en qué índice se encuentra
function numMay(tNums) {
    may = mayor(tNums);
    posicion = tNums.indexOf(may);
    document.getElementById('mayor').innerHTML = "Mayor: " + may + " Indice: " + posicion;
}

// Funciones para encontrar el número menor del arreglo y mostrar en qué índice se encuentra
function numMen(tNums) {
    men = menor(tNums);
    posicion = tNums.indexOf(men);
    document.getElementById('menor').innerHTML = "Menor: " + men + " Indice: " + posicion;
}

// Función para mostrar porcentaje de simetría, pares e impares
function porce(tNums) {
    porce = porcentajeSimetria(tNums);
    document.getElementById('simetrico').innerHTML = "Simétrico: " + porce.toFixed(2) + "%";
}

function porce_May_Men(tNums) {
    document.getElementById('porPares').innerHTML = "Porcentaje de pares: " + porcentajePares(tNums) + "%";
    document.getElementById('porImp').innerHTML = "Porcentaje de impares: " + porcentajeImpares(tNums) + "%";
}

// Declaración de la función para mostrar los números aleatorios
function mostrar() {
    // Obtener el elemento select
    const cmbNumeros = document.getElementById('cmbNumeros');
    // Eliminar todas las opciones del select
    while (cmbNumeros.firstChild) {
        cmbNumeros.removeChild(cmbNumeros.firstChild);
    }

    // Hacer el llenado y mostrar los resultados
    let rango = document.getElementById('range').value;
    let tNums = nums(rango);

    document.getElementById('promedio').innerHTML = "Promedio:" + promNumero(tNums).toFixed(2);
    // Esta función modifica directamente la etiqueta ya que requiere 2 valores para mostrar
    numMay(tNums);
    numMen(tNums);
    porce(tNums);
    porce_May_Men(tNums);
}

// Llamadas a las funciones
// mostrarArray(); // Puedes comentar esta línea si no deseas mostrar el arreglo original

var promedioValue = promedio(arreglo);
console.log("El promedio de los elementos del arreglo es: " + promedioValue.toFixed(2));

var numPar = pares(arreglo);
console.log("El arreglo contiene " + numPar + " números pares");

var numMayValue = mayor(arreglo);
console.log("El número mayor es: " + numMayValue);

console.log("Numeros random");

