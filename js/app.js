const btnCalular = document.getElementById('btnCalcular');

btnCalular.addEventListener('click', function(){
    let valorAuto = parseFloat(document.getElementById('txtValorAuto').value);
    let porcentaje = parseFloat(document.getElementById('txtPorcentaje').value);
    let plazo = parseInt(document.getElementById('plazos').value);

    
    let pagoInicial = valorAuto * (porcentaje/100);
    let totalFin = valorAuto - pagoInicial;
    let plazos = totalFin/plazo;

    
    document.getElementById('txtPagoInicial').value = pagoInicial;
    document.getElementById('txtTotalFin').value = totalFin;
    document.getElementById('txtPagoMensual').value = plazos;
});


const btnLimpiar = document.getElementById('btnLimpiar');


btnLimpiar.addEventListener('click', function(){
    document.getElementById('txtPagoInicial').value="";
    document.getElementById('txtTotalFin').value="";
    document.getElementById('txtPagoMensual').value="";
})
